<?php
function main($input)
{
  $weather_records = str_split($input);
  $output = 0;

  foreach($weather_records as $key => $record)
  {
      $weather_records[$key] = $record == 'S' ? 0 : 1;
  }

  extract($weather_records, EXTR_PREFIX_ALL, 'day');

  if(!isset($day_0) || !isset($day_1) || !isset($day_2))
  {
    exit();
  }

  if($day_0 && $day_1 && $day_2)
  {
      $output = 3;
  }
  elseif (($day_0 && $day_1) || ($day_1 && $day_2))
  {
      $output = 2;
  }
  elseif($day_0 || $day_1 || $day_2)
  {
      $output = 1;
  }

  fwrite(STDOUT, $output);
}

$input = trim(fgets(STDIN));
main($input);

// $tests = [
//   	'RRR',
//	 	'RRS',
//  	'SSS',
//  	'RSR'
// ];

// foreach($tests as $test)
// {
//	main($test);
// }
